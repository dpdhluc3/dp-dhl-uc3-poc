const storage = require("azure-storage");
const multer = require("multer");
const fs = require("fs");
const router = require("express").Router();
const sql = require("mssql");
const keys = require("../../config/key");

let containerName = "testing123";

/*Create instance of blob service */
const blobService = storage.createBlobServiceWithSas(
  keys.blobUri,
  keys.tempSASToken
);

function createBlobContainer() {
  console.log("Creating blob container.......");
  blobService.createContainerIfNotExists(containerName, (error, container) => {
    if (error) {
      console.log("Error creating blob container: ", error);
    } else {
      console.log(container.name);
    }
  });
}

router.post("/upload", (req, res) => {
  let storage = multer.diskStorage({
    destination: function(req, file, callback) {
      callback(null, "./server/storage");
    },
    filename: function(req, file, callback) {
      callback(null, file.originalname);
    }
  });
  let upload = multer({ storage: storage }).single("myCSV");
  upload(req, res, function(error) {
    if (error) {
      console.log(error);
      res.status(500).json({
        message: "Upload Failure", error: error,
        status: false });
    } else {
      const readStream = fs.createReadStream(
        "./server/storage/" + req.file.filename
      );
      blobService.createContainerIfNotExists(
        containerName,
        (error, container) => {
          if (error) {
            console.log("Error creating blob container: ", error);
            res.status(500).json({
              message: "Upload Failure", error: error,
              status: false });
          } else {
            console.log(container.name);
            readStream.pipe(
              blobService.createWriteStreamToBlockBlob(
                containerName,
                req.file.filename,
                (error, result, response) => {
                  if (error) {
                    console.log("Error writing in blob: ", error);
                    res
                      .status(500)
                      .json({
                        message: "Upload Failure", error: error,
                        status: false });
                  } else {
                    console.log(result, response);
                    //generateQry("./server/storage/" + req.file.filename, res);
                    sqlAdd(res, 'SELECT * FROM Uploadedfiledetails', req);
                  }
                }
              )
            );
          }
        }
      );
    }
  });
});

const sqlAdd = (res, query, req) => {
  sql.connect(
    keys.db,
    function(error) {
      if (error) {
        console.log("Error while connecting database :- " + error);
        res
          .status(500)
          .json({
            message: "Error while connecting database", error: error,
            status: false });
      } else {
        const request = new sql.Request()
        request.input("FileName", sql.NVarChar, req.file.filename);
        request.execute('LoadCSVToTable', (error, result1) => {
          if(error){
            res
              .status(500)
              .json({
                message: "Error while querying database",
                error: error,
                status:false
              });
          }else{
            request.query(query, function (error, result2) {
              if (error) {
                console.log("Error while querying database :- " + error);
                sql.close();
                res
                  .status(500)
                  .json({ message: "Error while querying database", error: error });
              } else {
                console.log("SQL res: ", result2);
                sql.close();
                res
                  .status(201)
                  .json({
                    message: "File uploaded in blob and added in sql table",
                    data: {uploadData:result1,filetable: result2, },
                    status:true
                  });
              }
            })
          }
        })
        /*request.query(query, function(error, result) {
          if (error) {
            console.log("Error while querying database :- " + error);
            res
              .status(500)
              .json({ message: "Error while querying database", error: error });
          } else {
            console.log("SQL res: ", result);
            res
              .status(201)
              .json({
                message: "File uploaded in blob and added in sql table",
                data: result
              });
          }
        });*/
      }
    }
  );
};

module.exports = router;

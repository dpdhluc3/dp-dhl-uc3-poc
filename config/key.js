const blobkeys={
  fnUri: "https://dpl-uc3.azurewebsites.net/api/uc3-file-fn",
  blobAccountName: "dhluc3",
  blobAccountKey:
    "64fGHO2RZhhzQ/zq/xfe5JVZjPpn7cWes7owC/8r0UVORsoNvjVO9d64b4lmj8YPP1LFwV40nDjMJKrvy5Cxzw=="
}
const sqlConfig = {
  user: "uc3", //dp-dhl-user01
  password: "Wipro@123", //password@1234
  server: "dhlsampledbuc3.database.windows.net",
  options: {
    encrypt: true
  },
  database: "DHLSampleDB_UC3"
};
module.exports = {
  ...blobkeys,
  db:{...sqlConfig},
  port: process.env.PORT||3000,
  blobUri: `https://${blobkeys.blobAccountName}.blob.core.windows.net`,
  tempSASToken:
    "se=2018-11-22&sp=rwdlac&sv=2018-03-28&ss=b&srt=sco&sig=ebF62eInXWuYCx1Qmz9j8RAP1lV09YLKUIFROO35uCc%3D",
  sasBash: `az storage account generate-sas --permissions racwdl --resource-types sco --services b --expiry 2018-11-22 --account-name ${
    blobkeys.blobAccountName
  } --account-key ${blobkeys.blobAccountKey}`
};